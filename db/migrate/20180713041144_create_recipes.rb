class CreateRecipes < ActiveRecord::Migration[5.2]
  def change
    create_table :recipes do |t|
      t.string :title, null: false # name of recipe
      t.string :making_time, null: false # time required to cook/bake the recipe
      t.string :serves, null: false # number of people the recipe will feed
      t.text :ingredients, null: false # food items necessary to prepare the recipe
      t.integer :cost, null: false # price of recipe
      t.timestamps null: false
    end
  end
end
